# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is used to store the source code of CA1: Memory Virtualization Simulation of Operating Systems module.

### What's inside the repository ###

root
|- data
|   |- disk_memory.txt
|   |- page_table.txt
|   |- physical_memory.txt
|- dist
|   |- simulate
|   |- simulate.o
|   |- utility.o
|- lib
|   |- utility.c
|   |-  utility.h
|- Makefile
|- simulate.c

### Compile command ###

make

### Run command ###

./dist/main

### Simulated physical/disk memory entry structure ###

Address   |Frame     |Content   |Memory Address Used
----------|----------|----------|-------------------
0x43D6    |67        |R         |1

### Page table entry structure ###

Page  |Frame  |Present Bit
------|-------|------------
0x00  |0x38   |1      

