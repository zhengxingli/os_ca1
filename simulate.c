#include <stdio.h>
#include <stdlib.h>
#include "lib/utility.h"

// use of char* and free() reference from lab exercise code: 
// https://2019-moodle.dkit.ie/pluginfile.php/560406/mod_resource/content/1/simple-malloc-1.c
int main()
{
    char *physical_memory = malloc(MEMORY_SIZE * sizeof(char));
    char *disk_memory = malloc(DISK_MEMORY_SIZE * sizeof(char));

    initialize_simulation(physical_memory, disk_memory);

    unsigned int input = 0;
    while (input != -1)
    {
        printf("\n\n-------------------------------------------------------------\n\n");
        printf("Please enter a hex address (-1 or Ctrl + C to exit) > ");
        scanf("%X", &input);
        if (input != -1)
        {
            translate_virtual_address(physical_memory, disk_memory, input);
        }
        else
        {
            printf("\n\nQuitting......\n\nBye!\n\n");
        }
        
    }

    free(physical_memory);
    free(disk_memory);
    return 0;
}