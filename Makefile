# makefile referenced and modified from code on Moodle: Makefile Version 2 (Variables)
CC      = gcc
CFLAGS  = -c -Wall
LIBDIR  = lib
DISTDIR  = dist
OBJECTS = $(DISTDIR)/simulate.o\
					$(DISTDIR)/utility.o
DATADIR = data

default: link

link: $(OBJECTS)
	$(CC) $? -o $(DISTDIR)/simulate

$(DISTDIR)/simulate.o: simulate.c
	$(CC) $(CFLAGS) $? -o $(DISTDIR)/simulate.o

$(DISTDIR)/utility.o: $(LIBDIR)/utility.c
	$(CC) $(CFLAGS) $? -o $(DISTDIR)/utility.o

clean:
	rm -rf ./$(DISTDIR) && mkdir $(DISTDIR) && rm -rf ./$(DATADIR) && mkdir $(DATADIR)
