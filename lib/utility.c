#include <stdio.h>
#include <stdlib.h>
#include "utility.h"
#include <stdbool.h>

char get_random_char()
{
    return (char)get_random_int(FIRST_PRINTABLE_CHAR, LAST_PRINTABLR_CHAR);
}

// Reference and modified from: https://www.tutorialspoint.com/c_standard_library/c_function_rand.htm
int get_random_int(int min, int max)
{
    return (rand() % (max - min)) + min;
}

void populate_data(char *arr)
{
    int data_size = get_random_int(MIN_DATA_SIZE, MAX_DATA_SIZE);
    int starting_frame = get_random_int(2, 178); 
    int starting_position = starting_frame * PAGE_SIZE;

    for (int i = starting_position; i < data_size + starting_position; ++i)
    {
        arr[i] = get_random_char();
    }
}

// write file function referenced from lab exercise code: https://2019-moodle.dkit.ie/pluginfile.php/560408/mod_resource/content/2/realloc.c
// Specifiers in fprintf() referenced from: https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm
void write_physical_memory_to_file(char *arr)
{
    FILE *file = fopen("./data/physical_memory.txt", "w");
    fprintf(file, "Address   |Frame     |Content   |Memory Address Used\n");
    fprintf(file, "----------|----------|----------|-------------------\n");

    for (int i = 0; i < MEMORY_SIZE; ++i)
    {
        int frame_number = i / PAGE_SIZE;
        if (arr[i] && i >= 2 * PAGE_SIZE)
        {
            fprintf(file, "0x%04X    |%-3d       |%c         |%d\n", i, frame_number, arr[i], true);
        }
        else
        {
            fprintf(file, "0x%04X    |%-3d       |          |%d\n", i, frame_number,false);
        }
    }

    fclose(file);
}

// write file function referenced from lab exercise code: https://2019-moodle.dkit.ie/pluginfile.php/560408/mod_resource/content/2/realloc.c
// Specifiers in fprintf() referenced from: https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm
void write_disk_memory_to_file(char *arr)
{
    FILE *file = fopen("./data/disk_memory.txt", "w");
    fprintf(file, "Address   |Frame     |Content   |Memory Address Used\n");
    fprintf(file, "----------|----------|----------|-------------------\n");

    for (int i = 0; i < DISK_MEMORY_SIZE; ++i)
    {
        short frame_number = i / PAGE_SIZE;

        if (arr[i])
        {
            fprintf(file, "0x%04X    |%-3d       |%c         |%d\n", i, frame_number, arr[i],true);
        }
        else
        {
            fprintf(file, "0x%04X    |%-3d       |          |%d\n", i, frame_number,false);
        }
    }

    fclose(file);
}

void generate_page_table(char *p_memory, char *d_memory)
{
    const int MEMORY_STARTING_FRAME = 2;
    int count = 0;
    for (int i = 0; i < (MEMORY_SIZE / PAGE_SIZE) - MEMORY_STARTING_FRAME; ++i)
    {
        int current_frame_number = i;
        if (check_empty_frame(current_frame_number, p_memory) == false)
        {
            p_memory[count] = current_frame_number;
            p_memory[count + PAGE_TABLE_ENTRIES] = CHAR_ONE;
            ++count;
        }
    }

    for (int i = 0; i < (DISK_MEMORY_SIZE / PAGE_SIZE); ++i)
    {
        if (check_empty_frame(i, d_memory) == false)
        {
            p_memory[count] = i;
            p_memory[count + PAGE_TABLE_ENTRIES] = CHAR_ZERO;

            ++count;
        }
    }
}

bool check_empty_frame(int frame_number, char *memory)
{
    int starting_position = frame_number * PAGE_SIZE;
    int ending_position = (frame_number + 1) * PAGE_SIZE;
    bool frame_empty = true;
    do
    {
        if (memory[starting_position])
        {
            frame_empty = false;
        }

        ++starting_position;
    } while (starting_position < ending_position && frame_empty);

    return frame_empty;
}

// write file function referenced from lab exercise code: https://2019-moodle.dkit.ie/pluginfile.php/560408/mod_resource/content/2/realloc.c
// Specifiers in fprintf() referenced from: https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm
void write_page_table_to_file(char *arr)
{
    FILE *file = fopen("./data//page_table.txt", "w");
    fprintf(file, "Page  |Frame  |Present Bit\n");
    fprintf(file, "------|-------|------------\n");

    for (int i = 0; i < PAGE_TABLE_ENTRIES; ++i)
    {
        if (arr[i])
        {
            fprintf(file, "0x%02X  |0x%02X   |%c      \n", i, (unsigned char)arr[i], arr[i + PAGE_TABLE_ENTRIES]);
        }
        else
        {
            fprintf(file, "0x%02X  |       |%c      \n", i, CHAR_ZERO);
        }
    }

    fclose(file);
}

// Specifiers in printf() referenced from: https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm
void initialize_simulation(char *physical_memory, char *disk_memory)
{
    printf("\nStarting memory virtualization simulation......\n");

    // Simulate physical memory
    printf("\nSimulating physical memory......");
    populate_data(physical_memory);
    write_physical_memory_to_file(physical_memory);
    printf("	Done!");

    // Simulate disk memory
    printf("\nSimulating disk memory......");
    for (int i = 1 * PAGE_SIZE; i < DISK_MEMORY_SIZE - PAGE_SIZE; ++i)
    {
        disk_memory[i] = get_random_char();
    }
    write_disk_memory_to_file(disk_memory);
    printf("	        Done!");

    // Generate page table, store it in first 2 frame of physical memory
    // and write to file
    printf("\nGenerating Page Table......");
    generate_page_table(physical_memory, disk_memory);
    write_page_table_to_file(physical_memory);
    printf("	        Done!");

    printf("\n\nExample of a page table entry\n");
    printf("\nPage  |Frame  |Present Bit\n");
    printf( "------|-------|------------\n");
    printf("0x%02X  |0x%02X   |%c      \n", 0, (unsigned char)physical_memory[0], physical_memory[0 + PAGE_TABLE_ENTRIES]);
}

// Reference: lab exercise code bitwise operation: 
// https://2019-moodle.dkit.ie/pluginfile.php/567340/mod_resource/content/2/32bit-address-parts.c
unsigned int get_page_number(unsigned int address)
{
    unsigned int vpn = address >> PAGE_BITS;
    return vpn;
}

// Reference: lab exercise code bitwise operation: 
// https://2019-moodle.dkit.ie/pluginfile.php/567340/mod_resource/content/2/32bit-address-parts.c
unsigned int get_offset(unsigned int address)
{
    unsigned int offset_mask = 0x00FF;
    unsigned int offset = address & offset_mask;

    return offset;
}

// Reference: lab exercise code bitwise operation: 
// https://2019-moodle.dkit.ie/pluginfile.php/567342/mod_resource/content/2/32bit-address-parts-reconstruct.c
unsigned int construct_physical_memory_address(unsigned int frame_number, unsigned int offset)
{
    unsigned int address = frame_number << OFFSET_BITS;
    address |= offset;

    return address;
}

char fetch_data_in_memory(char *memory, unsigned int physical_address)
{
    unsigned int frame_number = get_page_number(physical_address);
    unsigned int offset = get_offset(physical_address);
    int position = frame_number * PAGE_SIZE + offset;
    return memory[position];
}

void swap_frame(char *memory1, char *memory2, unsigned int memory1_frame_number, unsigned int memory2_frame_number)
{
    int memory1_starting_position = memory1_frame_number * PAGE_SIZE;
    int memory2_starting_position = memory2_frame_number * PAGE_SIZE;
    for (int i = 0; i < PAGE_SIZE; ++i)
    {
        int temp = memory1[memory1_starting_position + i];
        memory1[memory1_starting_position + i] = memory2[i + memory2_starting_position];
        memory2[i + memory2_starting_position] = temp;
    }
}

unsigned int get_page_number_by_frame_number(char *page_table, unsigned int frame_number)
{
    int index = 0;
    bool found = false;
    unsigned int page_number = 0;
    do
    {
        if (page_table[index] == frame_number)
        {
            page_number = index;
            found = true;
        }
        ++index;
    } while (index < PAGE_TABLE_ENTRIES && found == false);

    return page_number;
}

// Specifiers in printf() referenced from: https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm
void translate_virtual_address(char *physical_memory, char *disk_memory, unsigned int virtual_address)
{
    printf("\nStart translating address......\n ");
    unsigned physical_memory_address = 0x0000;
    printf("\n    - Address to be translated: 0x%04X\n", virtual_address);

    // Get page number and offset
    unsigned int page_number = get_page_number(virtual_address);
    unsigned int offset = get_offset(virtual_address);
    printf("\n    - Getting page number and offset from the given virtual address......\n           page: 0x%02X    offset: 0x%02X\n", page_number, offset);

    // Seek corresponding frame number in page table
    printf("\n    - Looking for corresponding frame # of page 0x%02X in page table......", page_number);

    if (physical_memory[page_number])
    {
        unsigned int frame_number = (unsigned char)physical_memory[page_number];
        char present_bit = physical_memory[page_number + PAGE_TABLE_ENTRIES];
        printf("\n           Page table entry found: Page 0x%02X -> Frame 0x%02X Present bit: %c.\n", page_number, frame_number, present_bit);

        if (present_bit == CHAR_ZERO)
        {
            printf("\n    - Page Fault Exception catched......\n");
            printf("\n    - Looking for page table to page out......");

            int pfn_to_page_out;
            int disk_frame_number_to_write;
            do
            {
                pfn_to_page_out = get_random_int(2, MEMORY_SIZE / PAGE_SIZE);
            } while (check_empty_frame(pfn_to_page_out, physical_memory) == true);

            do
            {
                disk_frame_number_to_write = get_random_int(1, DISK_MEMORY_SIZE / PAGE_SIZE);
            } while (check_empty_frame(disk_frame_number_to_write, disk_memory) == false);
            printf("\n           About to write frame 0x%02X to disk 0x%02X\n", pfn_to_page_out, disk_frame_number_to_write);

            printf("\n    - Paging out: writing frame 0x%02X to disk 0x%02X......", pfn_to_page_out, disk_frame_number_to_write);
            swap_frame(physical_memory, disk_memory, pfn_to_page_out, disk_frame_number_to_write);
            write_physical_memory_to_file(physical_memory);
            write_disk_memory_to_file(disk_memory);
            printf("                   Done!\n");

            printf("\n    - Paging in: writing missing frame 0x%02X into memory 0x%02X......", frame_number, pfn_to_page_out);
            swap_frame(disk_memory, physical_memory, frame_number, pfn_to_page_out);
            write_physical_memory_to_file(physical_memory);
            write_disk_memory_to_file(disk_memory);
            printf("        Done!\n");

            printf("\n    - Updating Page table entry......");
            physical_memory[page_number] = pfn_to_page_out;
            physical_memory[page_number + PAGE_SIZE] = CHAR_ONE;
            printf("\n            0x%02X: 0x%02X Present bit: %c  -> 0x%02X Present bit: %c", page_number, frame_number, present_bit, pfn_to_page_out, CHAR_ONE);

            unsigned int vpn = get_page_number_by_frame_number(physical_memory, pfn_to_page_out);
            physical_memory[vpn] = disk_frame_number_to_write;
            physical_memory[vpn + PAGE_SIZE] = CHAR_ZERO;
            printf("\n            0x%02X: 0x%02X Present bit: %c  -> 0x%02X Present bit: %c\n", vpn, pfn_to_page_out, CHAR_ONE, disk_frame_number_to_write, CHAR_ZERO);

            write_page_table_to_file(physical_memory);
            printf("            Page table updated.\n");

            printf("\n    - Continue address translation......");

            frame_number = physical_memory[page_number];
            present_bit = physical_memory[page_number + PAGE_SIZE];
        }

        printf("\n    - Constructing corresponding physical memory address......");
        physical_memory_address = construct_physical_memory_address(frame_number, offset);
        printf("\n           Corresponding physical memory address: 0x%04X\n", physical_memory_address);
        printf("\n    - Fetching data stored at this physical memory address......");
        char data = fetch_data_in_memory(physical_memory, physical_memory_address);
        printf("\n           Data stored at 0x%04X: %c\n", physical_memory_address, data);
        printf("\nTranslation process completed.");
    }
    else
    {
        printf("\n           Page table entry not found.\n");
        printf("           This address is not used yet.\n");
        printf("\nTranslation process terminated.");
    }
}
