# ifndef UTILITYH
# define UTILITYH
# define FIRST_PRINTABLE_CHAR 33
# define LAST_PRINTABLR_CHAR 126
# define PAGE_SIZE 256
# define MEMORY_SIZE  65536
# define PAGE_TABLE_ENTRIES 256
# define DISK_MEMORY_SIZE 1024
# define CHAR_ONE 49
# define CHAR_ZERO 48
# define OFFSET_BITS 8
# define PAGE_BITS 8
# define MAX_DATA_SIZE 20480
# define MIN_DATA_SIZE 2048


#include <stdbool.h>


	char get_random_char();
    int get_random_int(int, int);
    void populate_data(char*);
    void write_physical_memory_to_file(char*);
    void write_disk_memory_to_file(char*);
    void generate_page_table(char*, char*);
    bool check_empty_frame(int, char*);
    void write_page_table_to_file(char*);
    void initialize_simulation(char*, char*);
    unsigned int get_page_number(unsigned int);
    unsigned int get_offset(unsigned int);
    void translate_virtual_address(char*, char*, unsigned int);
    unsigned int construct_physical_memory_address(unsigned int , unsigned int );
    char fetch_data_in_memory(char*, unsigned int);
    void swap_frame(char*, char*, unsigned int, unsigned int);
    unsigned int get_page_number_by_frame_number(char*, unsigned int);

# endif


